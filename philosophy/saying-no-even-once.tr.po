# Turkish translation of https://www.gnu.org/philosophy/saying-no-even-once.html
# Copyright (C) 2020 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
#
# The FLOSS Information <theflossinformation@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: saying-no-even-once.html\n"
"POT-Creation-Date: 2021-09-05 10:26+0000\n"
"PO-Revision-Date: 2021-07-23 16:07+0200\n"
"Last-Translator: T. E. Kalayci <tekrei@member.fsf.org>\n"
"Language-Team: Turkish <www-tr-comm@gnu.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 2.4.2\n"

#. type: Content of: <title>
msgid ""
"Saying No to unjust computing even once is help - GNU Project - Free "
"Software Foundation"
msgstr ""
"Adaletsiz Bilişime Bir Kez Bile Hayır Demek Yararlıdır - GNU Projesi - Özgür "
"Yazılım Vakfı"

#. type: Content of: <div><h2>
msgid "Saying No to unjust computing even once is&nbsp;help"
msgstr "Adaletsiz Bilişime Bir Kez Bile Hayır Demek Yararlıdır"

#. type: Content of: <div><address>
msgid "by Richard Stallman"
msgstr "yazan Richard Stallman"

#. type: Content of: <div><p>
msgid ""
"A misunderstanding is circulating that the GNU Project demands you run 100% "
"<a href=\"/philosophy/free-sw.html\">free software</a>, all the time. "
"Anything less (90%?), and we will tell you to get lost&mdash;they say. "
"Nothing could be further from the truth."
msgstr ""
"GNU Projesi'nin her zaman %100 <a href=\"/philosophy/free-sw.html\">özgür "
"yazılım</a> çalıştırmanızı talep ettiği şeklinde bir yanlış anlama var. Daha "
"az özgür herhangi bir şey (%90?) söz konusu olduğunda size defol "
"diyebileceğimizi söylüyorlar. Gerçekten oldukça uzak bir söylem."

#. type: Content of: <div><p>
msgid ""
"Our ultimate goal is <a href=\"/philosophy/free-software-even-more-important."
"html\">digital freedom for all</a>, a world without nonfree software. Some "
"of us, who have made campaigning for digital freedom our goal, reject all "
"nonfree programs. However, as a practical matter, even a little step towards "
"that goal is good. A walk of a thousand miles consists of lots of steps. "
"Each time you don't install some nonfree program, or decide not to run it "
"that day, that is a step towards your own freedom. Each time you decline to "
"run a nonfree program with others, you show them a wise example of long-term "
"thinking. That is a step towards freedom for the world."
msgstr ""
"Nihai hedefimiz <a href=\"/philosophy/free-software-even-more-important.html"
"\">herkes için dijital özgürlük</a>, özgür olmayan yazılımsız bir dünya. "
"Dijital özgürlük için kampanya yapmayı hedefimiz haline getiren bazılarımız, "
"özgür olmayan tüm programları reddediyor. Ancak pratik anlamda bu hedefe "
"doğru küçük bir adım bile iyidir. Damlaya damlaya göl olur. Özgür olmayan "
"bir program kurmadığınız her seferinde veya o gün onu çalıştırmamaya karar "
"verdiğinizde, bu özgürlüğünüze doğru atılmış bir adımdır. Başkalarıyla özgür "
"olmayan bir program çalıştırmayı her reddettiğinizde onlara uzun vadeli "
"düşünmenin bilgili bir örneğini göstermiş olursunuz. Bu dünyanın özgürlüğüne "
"doğru bir adımdır."

#. type: Content of: <div><p>
msgid ""
"If you're caught in a web of nonfree programs, you're surely looking for a "
"chance to pull a few strands off of your body. Each one pulled off is an "
"advance."
msgstr ""
"Özgür olmayan programlardan oluşan bir ağa yakalanırsanız kesinlikle "
"gövdenizden birkaç tutamı koparma şansı arıyorsunuz. Her kopardığınız bir "
"tutam ilerlemedir."

#. type: Content of: <div><p>
msgid ""
"Each time you tell the people in some activity, &ldquo;I'd rather use Zoom "
"less&mdash;please count me out today,&rdquo; you help the free software "
"movement. &ldquo;I'd like to do this with you, but with Zoom on the other "
"side of the scale, I've decided to decline.&rdquo; If you accepted the "
"nonfree software before, you could say this: &ldquo;I'd like to participate, "
"but the software we are using is not good for us.  I've decided I should cut "
"down.&rdquo; Once in a while, you may convince them to use free software "
"instead. At least they will learn that some people care about freedom enough "
"to decline participation for freedom's sake."
msgstr ""
"Bir etkinlikte insanlara &ldquo;Zoom'u daha az kullanmayı yeğlerim&mdash;"
"lütfen bugün beni katmayın.&rdquo; dediğiniz her durum özgür yazılım "
"hareketine yardımcı olur. &ldquo;Bunu seninle yapmak istiyorum ancak diğer "
"taraftan bunu Zoom'la yapmayı reddetmeye karar verdim.&ldquo; Daha önce "
"özgür olmayan yazılımı kabul ettiyseniz şunu söyleyebilirsiniz: &ldquo;"
"Katılmak istiyorum ancak kullandığımız yazılım bizim için iyi değil. Onu "
"daha az kullanmam gerektiğine karar verdim.&rdquo; Arada bir onları özgür "
"yazılım kullanmaya ikna edebilirsiniz. En azından bazı insanların özgürlük "
"uğruna katılımı reddedecek kadar özgürlüğü önemsediğini öğrenecekler."

#. type: Content of: <div><p>
msgid ""
"If you say no, on one occasion, to conversing with someone or some group via "
"Skype, you have helped. If you say no, on one occasion, to conversing via "
"WhatsApp, Facebook, or Slack, you have helped. If you say no, on one "
"occasion, to editing something via Google Docs, you have helped. If you say "
"no to registering for one meeting in eventbrite.com or meetup.com, you have "
"helped. If you tell one organization you won't use its &ldquo;portal&rdquo; "
"or app, so you will deal with it by phone, that helps. Of course, you help "
"more if you stick to your refusal (with kind firmness, of course) and don't "
"let the others change your mind."
msgstr ""
"Bir sefer Skype aracılığıyla birisiyle veya bir grupla sohbete hayır "
"derseniz yardımcı olursunuz. Bir sefer WhatsApp, Facebook veya Slack "
"aracılığıyla sohbete hayır derseniz yardımcı olursunuz. Bir sefer Google "
"Dokümanlar aracılığıyla bir şeyi düzenlemeye hayır derseniz yardımcı "
"olursunuz. eventbrite.com'a veya meetup.com'a bir toplantı için kaydolmaya "
"hayır derseniz yardımcı olursunuz. Bir kuruluşa &ldquo;portal&rdquo;ını veya "
"uygulamasını kullanmayacağınızı söylerseniz, bunun yerine onlarla işinizi "
"telefonla görürseniz, bu yardımcı olur. Tabii ki reddetmenize bağlı "
"kalırsanız (kuşkusuz nazik bir katılıkla) ve başkalarının fikrinizi "
"değiştirmesine izin vermezseniz daha fazla yardımcı olmuş olursunuz."

#. type: Content of: <div><p>
msgid ""
"Steps add up. If on another day you decline the nonfree program again, you "
"will have helped again. If you say no a few times a week, that adds up over "
"time. When people see you say no, even once, you may inspire them to follow "
"your example."
msgstr ""
"Adımlar birikir. Başka bir gün özgür olmayan programı yeniden "
"reddettiğinizde yine yardımcı olmuş olacaksınız. Haftada birkaç kez hayır "
"derseniz, bu zamanla birikir. İnsanlar hayır dediğinizi gördüğünde, bu bir "
"kez olsa bile, sizi örnek almaları konusunda ilham vermiş olabilirsiniz."

#. type: Content of: <div><p>
msgid ""
"To give help consistently, you can make this refusal a firm practice, but "
"refusing occasionally is still help. You will help more if you reject "
"several of the nonfree programs that communities have blindly swallowed. "
"Would you ever want to reject them all? There is no need to decide that now."
msgstr ""
"Tutarlı bir şekilde yardım etmek için bu reddi sıkı bir bir uygulama hâline "
"getirebilirsiniz ancak arada sırada reddetmek de yardımcı olacaktır. "
"Toplulukların körü körüne kabul ettiği özgür olmayan programların birçoğunu "
"reddederseniz daha fazla yardım etmiş olursunuz. Onların hepsini reddetmek "
"ister misiniz? Buna şimdi karar vermeye gerek yok."

#. type: Content of: <div><p>
msgid ""
"So tell someone, &ldquo;Thanks for inviting me, but Zoom/Skype/WhatsApp/"
"whichever is a freedom-denying program, and almost surely snoops on its "
"users; please count me out. I want a different kind of world, and by "
"declining to use it today I am taking a step towards that world.&rdquo;"
msgstr ""
"Bu yüzden birine şunu söyleyin: &ldquo;Beni davet ettiğiniz için teşekkür "
"ederim ancak Zoom/Skype/WhatsApp/hangisi olursa olsun özgürlüğü reddeden bir "
"programdır ve kullanıcılarını hemen hemen kesinlikle gözetler; lütfen beni "
"katmayın. Ben başka bir dünya istiyorum ve bugün bunu kullanmayı reddederek "
"o dünyaya doğru bir adım atıyorum.&rdquo;"

#. type: Content of: <div><p>
msgid ""
"The FSF recommends <a href=\"https://www.fsf.org/blogs/community/better-than-"
"zoom-try-these-free-software-tools-for-staying-in-touch\"> freedom-"
"respecting methods</a> for the sorts of communication that unjust systems "
"do.  If one of them would be usable, you could add, &ldquo;If we use XYZ for "
"this conversation, or some other libre software, I could participate.&rdquo;"
msgstr ""
"FSF, adaletsiz sistemlerin gerçekleştirdiği her türlü iletişim yerine <a "
"href=\"https://www.fsf.org/blogs/community/better-than-zoom-try-these-free-"
"software-tools-for-staying-in-touch\">özgürlüğe saygılı yöntemler</a> "
"önerir. Bunlardan biri kullanılabilir olduğunda &ldquo;eğer bu konuşmayı XYZ "
"ile veya başka bir özgür yazılım ile yapacaksak katılabilirim.&rdquo; de "
"diyebilirsiniz."

#. type: Content of: <div><p>
msgid ""
"You can take one step. And once you've done it, sooner or later you can do "
"it again. Eventually you may find you have changed your practices; if you "
"get used to saying no to some nonfree program, you could do it most of the "
"time, maybe even every time. Not only will you have gained an increment of "
"freedom; you will have helped your whole community by spreading awareness of "
"the issue."
msgstr ""
"Bir adım atabilirsiniz. Ve o adımdan hemen sonra, er ya da geç, yeniden "
"yapabilirsiniz. Sonunda pratiklerinizi değiştirdiğinizi fark edebilirsiniz; "
"bazı özgür olmayan programlara hayır demeye alışırsanız, çoğu zaman, hatta "
"belki de her zaman bunu yapabilirsiniz. Yalnızca bir özgürlük çoğalması elde "
"etmekle kalmayacaksınız; konuyla ilgili farkındalık yaratarak tüm "
"topluluğunuza yardım etmiş olacaksınız."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Lütfen FSF ve GNU ile ilgili sorularınızı <a href=\"mailto:gnu@gnu.org\">&lt;"
"gnu@gnu.org&gt;</a> adresine iletin. FSF ile iletişim kurmanın <a href=\"/"
"contact/\">başka yolları</a> da vardır. Lütfen çalışmayan bağlantıları ve "
"başka düzeltmeleri veya önerilerinizi <a href=\"mailto:webmasters@gnu.org"
"\">&lt;webmasters@gnu.org&gt;</a> adresine gönderin."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Çevirilerimizde bulmuş olabileceğiniz hataları, aklınızdaki soru ve "
"önerilerinizi lütfen <a href=\"mailto:web-translators@gnu.org\">bize&nbsp;"
"bildirin</a>.</p><p>Bu yazının çeviri düzenlemesi ve sunuşu ile ilgili bilgi "
"için lütfen <a href=\"/server/standards/README.translations.html\">Çeviriler "
"BENİOKU</a> sayfasına bakın. Bu sayfanın ve diğer tüm sayfaların Türkçe "
"çevirileri gönüllüler tarafından yapılmaktadır; Türkçe niteliği yüksek bir "
"<a href=\"/home.html\">www.gnu.org</a> için bize yardımcı olmak "
"istiyorsanız, <a href=\"https://savannah.gnu.org/projects/www-tr"
"\">çalışma&nbsp;sayfamızı</a> ziyaret edebilirsiniz."

#. type: Content of: <div><p>
msgid "Copyright &copy; 2020, 2021 Richard Stallman"
msgstr "Copyright &copy; 2020, 2021 Richard Stallman"

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Bu sayfa <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-"
"nd/4.0/deed.tr\">Creative Commons Alıntı-Türetilemez 4.0 Uluslararası "
"Lisansı</a> altında lisanslanmıştır."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"<p><strong>Çeviriye katkıda bulunanlar:</strong></p>\n"
"<ul>\n"
"<li>The FLOSS Information <a href=\"mailto:theflossinformation@gmail.com"
"\">&lt;theflossinformation@gmail.com&gt;</a>, 2020.</li>\n"
"</ul>"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Son Güncelleme:"

#~ msgid "Copyright &copy; 2020 Richard Stallman"
#~ msgstr "Copyright &copy; 2020 Richard Stallman"
