# Turkish translation of https://www.gnu.org/licenses/fdl-1.3.html
# Copyright (C) 2020 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
#
# The FLOSS Information <theflossinformation@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: fdl-1.3.html\n"
"POT-Creation-Date: 2023-06-11 11:26+0000\n"
"PO-Revision-Date: 2023-06-17 09:59+0200\n"
"Last-Translator: T. E. Kalayci <tekrei@member.fsf.org>\n"
"Language-Team: Turkish <www-tr-comm@gnu.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.2.2\n"

#. type: Content of: <title>
msgid ""
"GNU Free Documentation License v1.3 - GNU Project - Free Software Foundation"
msgstr ""
"GNU Özgür Belgelendirme Lisansı v1.3 - GNU Projesi - Özgür Yazılım Vakfı"

#. type: Content of: <div><h2>
msgid "GNU Free Documentation License"
msgstr "GNU Özgür Belgelendirme Lisansı"

#. type: Attribute 'alt' of: <div><img>
msgid "[GFDL Logo]"
msgstr "[GFDL Logosu]"

#. type: Content of: <div><p>
msgid "<b><a href=\"#license-text\">Skip to license text</a></b>"
msgstr "<b><a href=\"#license-text\">Lisans metnine atla</a></b>"

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/licenses/fdl-1.3-faq.html\">FDL 1.3 Frequently Asked Questions</a>"
msgstr ""
"<a href=\"/licenses/fdl-1.3-faq.html\">FDL 1.3 Sıkça Sorulan Sorular</a>"

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/licenses/fdl-1.3-pdiff.ps\">Marked-up changes between FDL 1.2 and "
"FDL 1.3 (PostScript)</a>"
msgstr ""
"<a href=\"/licenses/fdl-1.3-pdiff.ps\">FDL 1.2 ve FDL 1.3 arasında belirgin "
"değişiklikler (PostScript)</a>"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/licenses/fdl-howto.html\">Tips on how to use the FDL</a>"
msgstr ""
"<a href=\"/licenses/fdl-howto.html\">FDL'nin nasıl kullanılacağına ilişkin "
"ipuçları</a>"

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/licenses/fdl-howto-opt.html\">How to Use the Optional Features of "
"the GFDL</a>"
msgstr ""
"<a href=\"/licenses/fdl-howto-opt.html\">GFDL'nin İsteğe Bağlı Özellikleri "
"Nasıl Kullanılır?</a>"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/licenses/translations.html\">Translations of the GFDL</a>"
msgstr "<a href=\"/licenses/translations.html\">GFDL'nin Çevirileri</a>"

#. type: Content of: <div><ul><li>
msgid ""
"The FDL in other formats: <a href=\"/licenses/fdl-1.3.txt\">plain text</a>, "
"<a href=\"/licenses/fdl-1.3.texi\">Texinfo</a>, <a href=\"/licenses/fdl-1.3-"
"standalone.html\">standalone HTML</a>, DocBook/XML <a href=\"/licenses/"
"fdl-1.3.xml\">v4</a> or <a href=\"/licenses/fdl-1.3-db5.xml\">v5</a>, <a "
"href=\"/licenses/fdl-1.3.tex\">LaTeX</a>, <a href=\"/licenses/fdl-1.3.md"
"\">Markdown</a>, <a href=\"/licenses/fdl-1.3.odt\">ODF</a>, <a href=\"/"
"licenses/fdl-1.3.rtf\">RTF</a>"
msgstr ""
"Diğer biçimlerde FDL: <a href=\"/licenses/fdl-1.3.txt\">düz metin</a>, <a "
"href=\"/licenses/fdl-1.3.texi\">Texinfo</a>, <a href=\"/licenses/fdl-1.3-"
"standalone.html\">müstakil HTML</a>, DocBook/XML <a href=\"/licenses/fdl-1.3."
"xml\">v4</a> veya <a href=\"/licenses/fdl-1.3-db5.xml\">v5</a>, <a href=\"/"
"licenses/fdl-1.3.tex\">LaTeX</a>, <a href=\"/licenses/fdl-1.3.md\">Markdown</"
"a>, <a href=\"/licenses/fdl-1.3.odt\">ODF</a>, <a href=\"/licenses/fdl-1.3."
"rtf\">RTF</a>"

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/licenses/why-gfdl.html\">Why publishers should use the GNU Free "
"Documentation License</a>"
msgstr ""
"<a href=\"/licenses/why-gfdl.html\">Yayıncılar neden GNU Özgür Belgelendirme "
"Lisansı'nı kullanmalıdır?</a>"

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/free-doc.html\">Free Software and Free Manuals</a>"
msgstr ""
"<a href=\"/philosophy/free-doc.html\">Özgür Yazılım ve Özgür Kılavuzlar</a>"

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/licenses/old-licenses/old-licenses.html#FDL\">Old versions of the "
"GNU Free Documentation License</a>"
msgstr ""
"<a href=\"/licenses/old-licenses/old-licenses.html#FDL\">GNU Özgür "
"Belgelendirme Lisansı'nın eski sürümleri</a>"

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/licenses/gpl-violation.html\">What to do if you see a possible "
"GFDL violation</a>"
msgstr ""
"<a href=\"/licenses/gpl-violation.html\">Olası bir GFDL ihlali görürseniz ne "
"yapmalısınız?</a>"

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Lütfen FSF ve GNU ile ilgili sorularınızı <a href=\"mailto:gnu@gnu.org\">&lt;"
"gnu@gnu.org&gt;</a> adresine iletin. FSF ile iletişim kurmanın <a href=\"/"
"contact/\">başka yolları</a> da vardır. Lütfen çalışmayan bağlantıları ve "
"başka düzeltmeleri veya önerilerinizi <a href=\"mailto:webmasters@gnu.org"
"\">&lt;webmasters@gnu.org&gt;</a> adresine gönderin."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Çevirilerimizde bulmuş olabileceğiniz hataları, aklınızdaki soru ve "
"önerilerinizi lütfen <a href=\"mailto:web-translators@gnu.org\">bize&nbsp;"
"bildirin</a>.</p><p>Bu yazının çeviri düzenlemesi ve sunuşu ile ilgili bilgi "
"için lütfen <a href=\"/server/standards/README.translations.html\">Çeviriler "
"BENİOKU</a> sayfasına bakın. Bu sayfanın ve diğer tüm sayfaların Türkçe "
"çevirileri gönüllüler tarafından yapılmaktadır; Türkçe niteliği yüksek bir "
"<a href=\"/home.html\">www.gnu.org</a> için bize yardımcı olmak "
"istiyorsanız, <a href=\"https://savannah.gnu.org/projects/www-tr"
"\">çalışma&nbsp;sayfamızı</a> ziyaret edebilirsiniz."

#. type: Content of: <div><p>
msgid "Copyright notice above."
msgstr "Telif hakkı bildirimi yukarıdadır."

#. type: Content of: <div><p>
msgid ""
"Everyone is permitted to copy and distribute verbatim copies of this license "
"document, but changing it is not allowed."
msgstr ""
"Herkes bu lisans belgesinin birebir kopyalarını kopyalama ve dağıtma "
"hakkında sahiptir ancak değiştirilemez."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"<p><strong>Çeviriye katkıda bulunanlar:</strong></p>\n"
"<ul>\n"
"<li>The FLOSS Information <a href=\"mailto:theflossinformation@gmail.com"
"\">&lt;theflossinformation@gmail.com&gt;</a>, 2020.</li>\n"
"</ul>"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Son Güncelleme:"
